#![allow(dead_code)]

use std::fs::File;
use std::io::prelude::*;
use std::path::Path;
use std::env;

extern crate time;


extern crate rand;
use rand::Rng;

extern crate sdl2;

use sdl2::pixels::Color;
use sdl2::pixels::PixelFormatEnum;
use sdl2::keyboard::Keycode;
use sdl2::event::Event;

const CHIP8_FONTSET: [u8;80] = [
    0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
    0x20, 0x60, 0x20, 0x20, 0x70, // 1
    0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
    0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
    0x90, 0x90, 0xF0, 0x10, 0x10, // 4
    0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
    0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
    0xF0, 0x10, 0x20, 0x40, 0x40, // 7
    0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
    0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
    0xF0, 0x90, 0xF0, 0x90, 0x90, // A
    0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
    0xF0, 0x80, 0x80, 0x80, 0xF0, // C
    0xE0, 0x90, 0x90, 0x90, 0xE0, // D
    0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
    0xF0, 0x80, 0xF0, 0x80, 0x80  // F
];

const CHIP8_GFX_WIDTH: usize = 64;
const CHIP8_GFX_HEIGHT: usize = 32;
const EMU_CLOCK_SPEED: i64 = 1000; // emulation speed
const CHIP8_KEYPAD_TBL: [sdl2::keyboard::Keycode;16] = [
    Keycode::Num1,Keycode::Num2,Keycode::Num3,Keycode::Num4,
    Keycode::Q,Keycode::W,Keycode::E,Keycode::R,
    Keycode::A,Keycode::S,Keycode::D,Keycode::F,
    Keycode::Z,Keycode::X,Keycode::C,Keycode::V ];

const DBG: bool = false;

macro_rules! dprintln {
    ($fmt:expr) => (if DBG { print!(concat!($fmt, "\n")) });
    ($fmt:expr, $($arg:tt)*) => (if DBG { print!(concat!($fmt, "\n"), $($arg)*) });
}

pub struct Chip8 {
    //opcode: u16,
    mem: [u8; 4096],
    v: [u8; 16],
    i: u16,
    pc: u16,
    stack: [u16; 16],
    sp: u8,
    gfx: [u8; CHIP8_GFX_WIDTH * CHIP8_GFX_HEIGHT],
    delay_timer: u8,
    sound_timer: u8,
    key: [u8; 16],
    draw_flag: bool,
}

fn dump_state(c8: &Chip8, cmdi: usize, opcode: u16) {
    let regs = (0..16).fold(String::new(), |a, v| format!("{}{}V{:x}={:x}",a, if v>0 { "," } else { "" }, v, c8.v[v]));
    let stack = (0..c8.sp).fold(String::new(), |a, v| format!("{}{}{:x}",a, if v>0 { "," } else { "" }, c8.stack[v as usize]));
    println!("{}**** {:x} {:x}", cmdi, c8.pc, opcode);
    println!("Registers: {}", regs);
    println!("Stack: {}", stack);
    println!("I: {:x}", c8.i);
}

fn debug_render(c8: &Chip8) {
	// Draw
	for y in (0..CHIP8_GFX_HEIGHT) {
		for x in (0..CHIP8_GFX_WIDTH) {
			if c8.gfx[(y*CHIP8_GFX_WIDTH) + x] == 0 {
				print!("O");
            }
			else {
				print!(" ");
            }
		}
		println!("");
	}
	println!("");
}


impl Chip8 {
    fn initialize() -> Chip8 {
        let mut c8 = Chip8 {
            //opcode: 0,
            mem: [0; 4096],
            v: [0; 16],
            i: 0,
            pc: 0x200,
            stack: [0; 16],
            sp: 0,
            gfx: [0; CHIP8_GFX_WIDTH * CHIP8_GFX_HEIGHT],
            delay_timer: 0,
            sound_timer: 0,
            key: [0; 16],
            draw_flag: true
        };
        for i in (0..80) {
            c8.mem[i] = CHIP8_FONTSET[i];
        }
        c8
    }
    
    // 00E0 - CLS (Clear the display.)
    fn op_00e0(&mut self) {
        for i in (0..CHIP8_GFX_WIDTH*CHIP8_GFX_HEIGHT) {
            self.gfx[i] = 0;
        }
        self.draw_flag = true;
        dprintln!("{:x}: CLS", self.pc);
        self.pc += 2;
    }


    // 00EE - RET Return from a subroutine.
    fn op_00ee(&mut self) {
        assert!(self.sp > 0);
        self.sp -= 1;
        dprintln!("{:x}: RET ->{:x}", self.pc, self.stack[self.sp as usize]);
        self.pc = self.stack[self.sp as usize];
        self.pc += 2;
        //self.stack[(self.sp) as usize] = 0; // not necessary stale stack cleanup
    }

    // 1nnn - JP addr (Jump to location nnn.)
    fn op_1000(&mut self, opcode: u16) {
        dprintln!("{:x}: JP {:x}", self.pc, opcode & 0x0FFF);
        self.pc = opcode & 0x0FFF;
    }

    // 2nnn: CALL (Call subroutine at nnn).
    fn op_2000(&mut self, opcode: u16) {
        self.stack[self.sp as usize] = self.pc;
        self.sp += 1;
        dprintln!("{:x}: CALL PC:{:x} -> {:x}", self.pc, self.stack[self.sp as usize], opcode & 0x0FFF);
        self.pc = opcode & 0x0FFF;
    }
    // 3xkk - SE Vx, byte (Skip next instruction if Vx = kk)
    // 4xkk - SNE Vx, byte
    fn op_30004000(&mut self, opcode: u16) {
        let x = ((opcode & 0x0F00) >> 8) as usize;
        let kk = (opcode & 0x00FF) as u8;
        let o = opcode & 0xF000;
        let op = if o == 0x3000 { "SE" } else { "SNE" };
        dprintln!("{:x}: {} V{}={:x}, {:x}", self.pc, op, x, self.v[x], kk);
        let cond = if op == "SE" { self.v[x] == kk } else { self.v[x] != kk };
        self.pc += if cond { 4 } else { 2 };
    }
    // 0x5xy0 - SE Vx, Vy (Skip next instruction if Vx = Vy.)
    fn op_5000(&mut self, opcode: u16) { 
        let x = ((opcode & 0x0F00) >> 8) as usize;
        let y = ((opcode & 0x00F0) >> 4) as usize;
        dprintln!("{:x}: SE V{}={:x}, V{}={:x}", self.pc, x, self.v[x], y, self.v[y]);
        self.pc += if self.v[x] == self.v[y] { 4 } else { 2 };
    }
    // 6xkk - LD Vx, kk (Set Vx = kk),
    // 7xkk - ADD Vx, byte (Set Vx = Vx + kk)
    fn op_60007000(&mut self, opcode: u16) {
        let x = ((opcode & 0x0F00) >> 8) as usize;
        let kk = (opcode & 0x00FF) as u8;
        let o = opcode & 0xF000;
        self.v[x] = if o == 0x6000 { kk } else { self.v[x].wrapping_add(kk) };
        self.v[0xF] = if (self.v[x] as u16) + (kk as u16) > 0xFF { 1 } else { 0 };
        dprintln!("{:x}: {} V{}, {} = {}", self.pc, if o == 0x6000 { "LD" } else { "ADD" }, x, kk, self.v[x]);
        self.pc += 2;
    }

    fn op_8000(&mut self, opcode: u16) {
        let x = ((opcode & 0x0F00) >> 8) as usize;
        let y = ((opcode & 0x00F0) >> 4) as usize;
        match opcode & 0x000F {
            0x0 => { // 8xy0 - LD Vx, Vy (Set Vx = Vy.)
                self.v[x] = self.v[y];
                dprintln!("{:x}: LD V{}, V{} -> {:x}", self.pc, x, y, self.v[x]);
            },
            0x1 => { // 8xy1 - OR Vx, Vy (Set Vx = Vx OR Vy.)
                self.v[x] |= self.v[y];
                dprintln!("{:x}: OR V{}, V{} -> {:x}", self.pc, x, y, self.v[x]);
            },
            0x2 => { // 8xy2 - AND Vx, Vy (Set Vx = Vx AND Vy.)
                self.v[x] &= self.v[y];
                dprintln!("{:x}: AND V{}, V{} -> {:x}", self.pc, x, y, self.v[x]);
            },
            0x3 => { // 8xy3 - XOR Vx, Vy (Set Vx = Vx XOR Vy.)
                self.v[x] ^= self.v[y];
                dprintln!("{:x}: XOR V{}, V{} -> {:x}", self.pc, x, y, self.v[x]);
            },
            0x4 => { // 8xy4 - ADD Vx, Vy (Set Vx = Vx + Vy, set VF = carry.)
                self.v[0xF] = if self.v[y] > (0xFF - self.v[x]) { 1 } else { 0 };
                self.v[x].wrapping_add(self.v[y]);
                dprintln!("{:x}: ADD V{}, V{} -> {:x}", self.pc, x, y, self.v[x]);
            },
            0x5 => { // 8xy5 - SUB Vx, Vy (Set Vx = Vx - Vy, set VF = NOT borrow.)
                self.v[0xF] = if self.v[y] > self.v[x] { 0 } else { 1 };
                self.v[x].wrapping_sub(self.v[y]);
                dprintln!("{:x}: SUB V{}, V{} -> {:x}", self.pc, x, y, self.v[x]);
            },
            0x6 => { // 8xy6 - SHR Vx {, Vy} (Set Vx = Vx SHR 1.)
                self.v[0xF] = self.v[x] & 0x1;
                self.v[x] >>= 1;
                dprintln!("{:x}: SHR V{} -> {:x}", self.pc, x, self.v[x]);
            },
            0x7 => { // 8xy7 - SUBN Vx, Vy (Set Vx = Vy - Vx, set VF = NOT borrow.)
                self.v[0xF] = if self.v[x] > self.v[y] { 0 } else { 1 };
                self.v[x] = self.v[y] - self.v[x];
                dprintln!("{:x}: SUBN V{}, V{} -> {:x}", self.pc, x, y, self.v[x]);
            },
            0xE => { //  8xyE - SHL Vx {, Vy} (Set Vx = Vx SHL 1.)
                self.v[0xF] = self.v[x] >> 7;
                self.v[x] = self.v[x] << 1;
                dprintln!("{:x}: SHL V{} -> {:x}", self.pc, x, self.v[x]);
            },
            _ => panic!("Unknown opcode {:x}", opcode)
        }
        self.pc += 2;
    }

    fn op_9000(&mut self, opcode: u16) { //  SNE Vx, Vy (Skip next instruction if Vx != Vy.)
        let x = ((opcode & 0x0F00) >> 8) as usize;
        let y = ((opcode & 0x00F0) >> 4) as usize;
        dprintln!("{:x}: SNE V{}={:x}, V{}={:x}", self.pc, x, self.v[x], y, self.v[y]);
        self.pc += if self.v[x] != self.v[y] { 4 } else { 2 };
    }

    fn op_a000(&mut self, opcode: u16) { // Annn - LD I, nnn (Sets I to the address NNN)
        self.i = opcode & 0x0FFF;
        dprintln!("{:x}: LD I, {:x}", self.pc, self.i);
        self.pc += 2;
    }
    fn op_b000(&mut self, opcode: u16) { // Bnnn - JP V0, nnn (Jumps to the address NNN plus V0)
        let addr = opcode & 0x0FFF;
        self.pc = addr + self.v[0] as u16;
    }
    fn op_c000(&mut self, opcode: u16) { // Cxkk - RND Vx, byte  (Set Vx = random byte AND kk.)
        let x = ((opcode & 0x0F00) >> 8) as usize;
        let kk = (opcode & 0x00FF) as u8;
        let r = rand::thread_rng().gen::<u8>();
        //let r : u8 = 10; //TODO
        self.v[x] = r & kk;
        dprintln!("{:x}: RND V{}, {:x}", self.pc, x, r & kk);
        self.pc += 2;
    }
    fn op_d000(&mut self, opcode: u16) { // Dxyn - DRW Vx, Vy, nibble
        let n = (opcode & 0x000F) as usize;
        let from = self.i as usize;
        let to = from + n;
        let x = self.v[((opcode & 0x0F00) >> 8) as usize] as usize;
        let y = self.v[((opcode & 0x00F0) >> 4) as usize] as usize;
        let sprite: &[u8] = &self.mem[from..to];
        self.v[0xF] = 0;

        for py in (0..n) {
            for px in (0..8) {
                let gfx_coord = x + px + (y + py) * CHIP8_GFX_WIDTH;
                if gfx_coord >= self.gfx.len() { continue };
                if (sprite[py] & (0x80 >> px)) != 0 {
                    if self.gfx[gfx_coord] == 1 { self.v[0xF] = 1 };
                    self.gfx[gfx_coord] ^= 1;
                }
            }
        }
        dprintln!("{:x}: DRW V{}={}, V{}={}, N{} (I={:x})", self.pc, ((opcode & 0x0F00) >> 8), x, ((opcode & 0x00F0) >> 4), y, n, self.i);
        self.draw_flag = true;
        self.pc += 2;
    }
    fn op_e000(&mut self, opcode: u16) { // ExA1 - SKNP Vx, Ex9E - SKP Vx
        // Skip next instruction if key with the value of Vx is (not) pressed.
        let x = ((opcode & 0x0F00) >> 8) as usize;
        let etype = if (opcode & 0x00FF) == 0x9E { "SKP" } else { "SKNP" };
        let cond = if etype == "SKP" { 1 } else { 0 };
        dprintln!("{:x}: {} key[{}]={}", self.pc, etype, self.v[x], self.key[self.v[x] as usize]);
        self.pc += if self.key[self.v[x] as usize] == cond { 4 } else { 2 };
    }

    fn op_f000(&mut self, opcode: u16) {
        let x = ((opcode & 0x0F00) >> 8) as usize;
        match opcode & 0x00FF {
            0x07 => { // Fx07 - LD Vx, DT (Set Vx = delay timer value.)
                self.v[x] = self.delay_timer;
                dprintln!("{:x}: LD V{}, DT={}", self.pc, x, self.delay_timer);
                self.pc += 2;
            },
            0x0A => { // Fx0A - LD Vx, K (Wait for a key press, store the value of the key in Vx.)
                let mut key_pressed = false;
                for i in (0..16) {
                    if self.key[i] == 1 {
                        self.v[x] = i as u8;
                        key_pressed = true;
                    }
                }
                if key_pressed {
                    dprintln!("{:x}: LD V{}, K={}", self.pc, x, self.v[x]);
                    self.pc += 2;
                }
            },
            0x15 => { // Fx15 - LD DT, Vx (Set delay timer = Vx.)
                self.delay_timer = self.v[x];
                dprintln!("{:x}: LD DT, V{}={:x}", self.pc, x, self.v[x]);
                self.pc += 2;
            },
            0x18 => { // Fx18 - LD ST, Vx (Set sound timer = Vx.)
                self.sound_timer = self.v[x];
                dprintln!("{:x}: LD ST, {:x}", self.pc, self.sound_timer);
                self.pc += 2;
            },
            0x1E => { // Fx1E - ADD I, Vx (Set I = I + Vx.)
                let sum = self.i + (self.v[x] as u16);
                // VF is set to 1 when range overflow (I+VX>0xFFF), and 0 when there isn't.
                self.v[0xF] = if sum > 0xFFF { 1 } else { 0 };
                self.i = sum;
                dprintln!("{:x}: ADD I, V{}={:x} ->I={:x}", self.pc, x, self.v[x], self.i);
                self.pc += 2;
            },
            0x29 => { // Fx29 - LD F, Vx (Set I = location of sprite for digit Vx.)
                // Characters 0-F (in hexadecimal) are represented by a 4x5 font
                self.i = (self.v[x] as u16)*0x5;
                dprintln!("{:x}: LD F, V{}={:x} ->I={:x}", self.pc, x, self.v[x], self.i);
                self.pc += 2;
            },
            0x33 => { // Fx33 - LD B, Vx (Store BCD representation of Vx in memory locations I, I+1, and I+2.)
                let ind = self.i as usize;
                self.mem[ind]     = self.v[x] / 100;
                self.mem[ind + 1] = (self.v[x] / 10) % 10;
                self.mem[ind + 2] = (self.v[x] % 100) % 10;					
                dprintln!("{:x}: LD B, V{}={:x}", self.pc, x, self.v[x]);
                self.pc += 2;
            },
            0x55 => { // Fx55 - LD [I], Vx (Store registers V0 through Vx in memory starting at location I.)
                for i in (0..x+1) {
                    self.mem[self.i as usize + i] = self.v[i];
                }
                // On the original interpreter, when the operation is done, I = I + X + 1.
                self.i += (x as u16) + 1;

                dprintln!("{:x}: LD [I], V{}={:x}", self.pc, x, self.v[x]);
                self.pc += 2;
            },
            0x65 => { // Fx65 - LD Vx, [I] (Read registers V0 through Vx from memory starting at location I.)
                for i in (0..x+1) {
                    self.v[i] = self.mem[self.i as usize + i];
                }
                // On the original interpreter, when the operation is done, I = I + X + 1.
                self.i += (x as u16) + 1;
                dprintln!("{:x}: LD V{}={:x}, [I]", self.pc, x, self.v[x]);
                self.pc += 2;
            },
            _ => panic!("Unknown opcode {:x}", opcode)
        }
    }

    fn emulate_cycle(&mut self) -> u16 {
        let opcode : u16 = ((self.mem[self.pc as usize] as u16) << 8u16) | (self.mem[(self.pc+1) as usize] as u16);
        //println!("{}?, {:x}", self.pc, opcode);

        match opcode & 0xF000 {
            0x0000 => {
                match opcode {
                    0x00E0 => self.op_00e0(),
                    0x00EE => self.op_00ee(),
                    _ => panic!("Unknown opcode {:x}", opcode)
                }
            }
            0x1000 => self.op_1000(opcode),
            0x2000 => self.op_2000(opcode),
            0x3000 | 0x4000 => self.op_30004000(opcode),
            0x5000 => self.op_5000(opcode),
            0x6000 | 0x7000 => self.op_60007000(opcode),
            0x8000 => self.op_8000(opcode),
            0x9000 => self.op_9000(opcode),
            0xA000 => self.op_a000(opcode),
            0xB000 => self.op_b000(opcode),
            0xC000 => self.op_c000(opcode),
            0xD000 => self.op_d000(opcode),
            0xE000 => self.op_e000(opcode),
            0xF000 => self.op_f000(opcode),
            _ => panic!("Unknown opcode {:x}", opcode)
        }
        opcode
    }

    fn load_bin(&mut self, fname: &str) {
        let path = Path::new(fname);
        //let mut file : File = try!(File::open(&path));
        let file = match File::open(&path) {
            Ok(f) => f,
            Err(e) => panic!("error occured opening file: '{}' ({})!", fname, e)
        };
        //let mut buf : Vec<u8>;
        //try!(file.read_to_end(&mut buf));
        let biter = file.bytes();
        let mut idx : usize = 512;
        for b in biter {
            self.mem[idx] = b.unwrap();
            idx += 1;
        }
    }
}

fn sdl_draw_graphics<'a>(renderer: &mut sdl2::render::Renderer<'a>, gfx: &[u8]) {
    let mut texture = renderer.create_texture_streaming(PixelFormatEnum::RGB332, (CHIP8_GFX_WIDTH as u32, CHIP8_GFX_HEIGHT as u32)).unwrap();
    texture.with_lock(None, |buffer: &mut [u8], _pitch: usize| {
        let mut offset = 0;
        for p in gfx {
            buffer[offset] = if *p == 0 { 0 } else { 255 };
            offset += 1;
        }
    }).unwrap();

    renderer.clear();
    renderer.copy(&texture, None, None);
    renderer.present();
}


fn init_sdl<'a>() -> (sdl2::Sdl, sdl2::render::Renderer<'a>) {
    let sdl_context = sdl2::init().video().unwrap();

    let window = sdl_context.window("rust-sdl2 demo: Video", 800, 600)
        .position_centered()
        .opengl()
        .build()
        .unwrap();

    let mut renderer = window.renderer().build().unwrap();

    renderer.set_draw_color(Color::RGB(0, 0, 0));
    renderer.clear();
    renderer.present();
    (sdl_context, renderer)
}

fn sdl_ask_keys(sdl_context: &mut sdl2::Sdl, my_chip8: &mut Chip8) -> bool {
    for event in sdl_context.event_pump().poll_iter() {
        match event {
            Event::Quit {..} | Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
                return false;
            },
            Event::KeyDown { keycode: Some(ref keycode), .. } => {
                // if let Some(i) = CHIP8_KEYPAD_TBL.position_elem(keycode) {
                if let Some((i,_)) = (0..).zip(CHIP8_KEYPAD_TBL.iter()).filter(|&(_,x)| x == keycode).next() {
                    my_chip8.key[i] = 1;
                }
            },
            Event::KeyUp { keycode: Some(ref keycode), .. } => {
                // if let Some(i) = CHIP8_KEYPAD_TBL.position_elem(keycode) {
                if let Some((i,_)) = (0..).zip(CHIP8_KEYPAD_TBL.iter()).filter(|&(_,x)| x == keycode).next() {
                    my_chip8.key[i] = 0;
                }
            },
            _ => {}
        }
    }
    true
}

pub fn main() {

    let args: Vec<_> = env::args().collect();
    if args.len() < 2 {
        panic!("Usage: {} chip8_program", args[0]);
    }
    
    let (mut sdl_context, mut renderer) = init_sdl();

    let mut running = true;

    // time
    //let clock_counter : u64 = 0;
    //let clock_counter60 : u64 = 0;
    let mut prev_now = time::get_time();
    let mut prev_now60 = time::get_time();
    let time_frame : i64 = 1000 / EMU_CLOCK_SPEED;
    let time_frame60 : i64 = 1000 / 60;

    // init chip8
    let mut my_chip8 = Chip8::initialize();
    //my_chip8.load_bin("TETRIS");
    my_chip8.load_bin(&args[1]);

    //let mut cycle_cnt : usize = 0;
    while running {
        // timing
        let now = time::get_time();
        // update counter for emulator clock
        if (now - prev_now).num_milliseconds() >= time_frame {
            //clock_counter += 1;
            prev_now = now;

            //
            // chip 8 emulation
            let _opcode = my_chip8.emulate_cycle();

            //dump_state(&my_chip8, cycle_cnt, opcode); // TODO
            //cycle_cnt += 1;
            
            if my_chip8.draw_flag {
                //debug_render(&my_chip8);
                sdl_draw_graphics(&mut renderer, &my_chip8.gfx);
                my_chip8.draw_flag = false;
            }
            //
            running = sdl_ask_keys(&mut sdl_context, &mut my_chip8);
        }

        // update counter for 60Hz clock
        if (now - prev_now60).num_milliseconds() >= time_frame60 {
            //clock_counter60 += 1;
            prev_now60 = now;

            // CHIP-8 60Hz timers
            if my_chip8.delay_timer > 0 {
                my_chip8.delay_timer -= 1;
            }
            
            if my_chip8.sound_timer > 0 {
                if my_chip8.sound_timer == 1 {
                    println!("BEEP!\n");
                }
                my_chip8.sound_timer -= 1;
            }

        }
        //let to_sleep = time_frame - diff;
        //if to_sleep > 0 { thread::sleep_ms(to_sleep as u32) };
    }
}



#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn call_return() {
        let mut c8 = Chip8::initialize();
        // call
        c8.op_2000(0x22c8);
        assert_eq!(c8.pc, 0x2c8);
        assert_eq!(c8.sp, 0x1);
        assert_eq!(c8.stack[0], 0x200);
        // return
        c8.op_00ee();
        assert_eq!(c8.pc, 0x202);
        assert_eq!(c8.sp, 0);
    }

    #[test]
    fn se_30004000() {
        let mut c8 = Chip8::initialize();
        // not skip instruction if v[1] != 0x22
        c8.op_30004000(0x3122);
        assert_eq!(c8.pc, 0x202);
        // 0x4 - inverse op
        c8.op_30004000(0x4122);
        assert_eq!(c8.pc, 0x206);
        // skip instruction now because 0x22 loaded into v[1]
        c8.v[1] = 0x22;
        c8.op_30004000(0x3122);
        assert_eq!(c8.pc, 0x20A);
        // 0x4 - inverse op
        c8.op_30004000(0x4122);
        assert_eq!(c8.pc, 0x20C);
    }

    #[test]
    fn se_5000() {
        let mut c8 = Chip8::initialize();
        c8.v[1] = 1;
        c8.v[2] = 1;
        // should skip instruction if v[1] = v[2]
        c8.op_5000(0x5120);
        assert_eq!(c8.pc, 0x204);
        // should not skip instruction now
        c8.v[2] = 2;
        c8.op_5000(0x5120);
        assert_eq!(c8.pc, 0x206);
    }

    #[test]
    fn ld_60007000() {
        let mut c8 = Chip8::initialize();
        // 6xkk: LD Vx, kk (Set Vx = kk)
        c8.op_60007000(0x6102);
        assert_eq!(c8.v[1], 0x2);
        //  7xkk - ADD Vx, byte (Set Vx = Vx + kk)
        c8.op_60007000(0x7102);
        assert_eq!(c8.v[1], 0x4);
        //  overflow
        c8.op_60007000(0x71FF);
        assert_eq!(c8.v[1], 0x3);
        assert_eq!(c8.v[0xF], 0x1);
    }
}


